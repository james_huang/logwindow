package com.jameshuang.logwindowdemo

import android.app.Activity
import android.os.Bundle
import android.util.Log
import com.jameshuang.logwindow.LogWindowLauncher
import kotlinx.android.synthetic.main.activity_main.*

/**
 * @auther James Huang
 * @date 2018/12/18
 */
class MainActivity : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnLog.setOnClickListener { v ->
            Log.e("RRR", "click           a           Log")
        }

        btnCrash.setOnClickListener { v ->
            val obj: Any? = null
            obj!!
        }

        LogWindowLauncher.launch(this)
    }
}