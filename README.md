# LogWindow

---------

LogWindow is a tool to monitor the log for your application.

It can help you when the device doesn't connect with the console, and have some simple options and filter to optimize the reading.

# Download

---------

project gradle:

```groovy
repositories {
    maven {
        url 'https://dl.bintray.com/jameshuang/LogWindow'
    }
}
```

app gradle:

```groovy
dependencies {
   ...
   implementation 'com.jameshuang.logwindow:logwindow:1.0.3@aar'
}
```

# Usage

-----

Call following static function to launch the floating window.

You can add another launcher activity to implement this function, that would help you to easier enter the debug mode, and without changing the existed architecture.

```kotlin
LogWindowLauncher.launch(context: Context)
```

![alt text](mdpic/window.png)

#### Options Button

![alt text](mdpic/btn_settings.png)

There are 4 options to set the display:

* Time: Display the log time.

* PID and TID: Display the process ID and thread ID.

* Type: Display the log type. i.e. VERBOSE (V), INFO (I) ... etc.

* Tag: Display the log tag.

#### Filter Button

![alt text](mdpic/btn_filter.png)
![alt text](mdpic/btn_filter_hl.png)

You can specified the log type or the string to filter the log.
When the filter is working, the button will highlight.

#### Bottom Button

![alt text](mdpic/btn_bottom.png)

LogWindow will scroll down automatically until you touch the window, click the button can enable the auto-scrolling again.

#### Clean Button

![alt text](mdpic/btn_clean.png)

Clean button can clean the current display.

#### Close Button

![alt text](mdpic/btn_close.png)

Click the close button to shutdown the window. You need to call the API to relaunch the window.