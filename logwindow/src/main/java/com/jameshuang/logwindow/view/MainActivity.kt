package com.jameshuang.logwindow.view

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import com.jameshuang.logwindow.service.LoggerService

/**
 * @auther James Huang
 * @date 2018/12/14
 */
internal class MainActivity : Activity() {

    private val SCHEME_PACKAGE = "package:"
    private val REQ_SETTINGS = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (Settings.canDrawOverlays(this)) {
            showLogWindow()
        } else {
            showSettingsPage()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            REQ_SETTINGS -> {
                if (Settings.canDrawOverlays(this)) {
                    showLogWindow()
                } else {
                    showSettingsPage()
                }
            }
        }
    }

    private fun showSettingsPage() {
        val intent = Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse(SCHEME_PACKAGE + packageName))
        startActivityForResult(intent, REQ_SETTINGS)
    }

    private fun showLogWindow() {
        val intent = Intent(this, LoggerService::class.java)
        val bundle = Bundle()
        bundle.putInt(LoggerService.KEY_CMD, LoggerService.CMD_START)
        intent.putExtras(bundle)

        startService(intent)
        finish()
    }
}