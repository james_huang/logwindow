package com.jameshuang.logwindow

import android.content.Context
import android.content.Intent
import com.jameshuang.logwindow.view.MainActivity

/**
 * @auther James Huang
 * @date 2018/12/17
 */
open class LogWindowLauncher {
    companion object {
        fun launch(context: Context) {
            val intent = Intent(context, MainActivity::class.java)
            context.startActivity(intent)
        }
    }
}