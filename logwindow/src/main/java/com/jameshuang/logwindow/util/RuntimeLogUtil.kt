package com.jameshuang.logwindow.util

import java.io.BufferedReader
import java.io.InputStreamReader

/**
 * @auther James Huang
 * @date 2018/12/14
 */
internal class RuntimeLogUtil {

    private val CMD_LOGCAT = "logcat"
    private val CMD_LOGCAT_CLEAN = "logcat -c"

    private var mRuntimeThread: RuntimeThread? = null
    private var mListener: RuntimeLogListener? = null

    private constructor(listener: RuntimeLogListener) {
        mListener = listener
    }

    fun start() {
        if (mRuntimeThread == null) {
            mRuntimeThread = RuntimeThread()
        }

        mRuntimeThread?.startThread()
    }

    fun stop() {
        if (mRuntimeThread == null) {
            mRuntimeThread = RuntimeThread()
        }

        mRuntimeThread?.stopThread()
    }

    fun clearCache(listener: RequestListener?) {
        Thread {
            var process: Process? = null
            try {
                process = Runtime.getRuntime().exec(CMD_LOGCAT_CLEAN)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            process?.destroy()
            listener?.onRequestDone()
        }.start()
    }

    fun setListener(listener: RuntimeLogListener) {
        mListener = listener
    }

    class Builder {
        companion object {
            @Volatile
            private var mInstance: RuntimeLogUtil? = null
        }

        fun build(listener: RuntimeLogListener): RuntimeLogUtil {
            if (mInstance != null) {
                mInstance!!.setListener(listener)
                return mInstance!!
            } else {
                mInstance = RuntimeLogUtil(listener)
                return mInstance!!
            }
        }
    }

    inner class RuntimeThread : Thread() {
        private var mIsRunning = false

        fun startThread() {
            if (mIsRunning) {
                return
            }

            mIsRunning = true
            start()
        }

        fun stopThread() {
            if (!mIsRunning) {
                return
            }

            mIsRunning = false
            try {
                interrupt()
            } catch (e: Exception) {
                e.printStackTrace()
            }
            mRuntimeThread = null
        }

        override fun run() {
            var process: Process? = null
            var reader: BufferedReader? = null
            try {
                process = Runtime.getRuntime().exec(CMD_LOGCAT)
                reader = BufferedReader(InputStreamReader(process.inputStream))

                var msg: String? = ""
                while (mIsRunning) {
                    msg = reader.readLine()
                    if (msg != null) {
                        Thread {
                            mListener?.onReadLine(msg)
                        }.start()
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }

            reader?.close()
            process?.destroy()
        }
    }

    internal interface RuntimeLogListener {
        fun onReadLine(msg: String)
    }

    internal interface RequestListener {
        fun onRequestDone()
    }
}