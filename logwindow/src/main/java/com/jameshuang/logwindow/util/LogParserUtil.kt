package com.jameshuang.logwindow.util

/**
 * @auther James Huang
 * @date 2018/12/14
 */
internal class LogParserUtil {

    companion object {

        private val SYMBOL_SPACE = " "
        private val SYMBOL_COLON = ":"

        fun parse(msg: String): LogMessage? {
            val ret = LogMessage()
            try {
                val list = msg.split(SYMBOL_SPACE)
                val firstPos = findIndex(list, 0)
                val secondPos = findIndex(list, firstPos + 1)
                val pidPos = findIndex(list, secondPos + 1)
                val tidPos = findIndex(list, pidPos + 1)
                val typePos = findIndex(list, tidPos + 1)
                val tagPos = findIndex(list, typePos + 1)
                ret.wholeMsg = msg
                ret.time = list[firstPos] + SYMBOL_SPACE + list[secondPos]
                ret.pid = list[pidPos]
                ret.tid = list[tidPos]
                ret.type = list[typePos]
                ret.tag = list[tagPos]

                val msgPos: Int
                if (ret.tag.contains(SYMBOL_COLON)) {
                    msgPos = findIndex(list, tagPos + 1)
                } else {
                    ret.tag += SYMBOL_COLON
                    val colonPos = findIndex(list, tagPos + 1)
                    msgPos = findIndex(list, colonPos + 1)
                }
                for (i in msgPos until list.size) {
                    ret.msg += list[i] + SYMBOL_SPACE
                }
            } catch (e: Exception) {
                return null
            }

            return ret
        }

        private fun findIndex(list: List<String>, start: Int): Int {
            var ret = start
            for (i in start until list.size) {
                if (!list[i].matches(Regex(" +")) && !list[i].isEmpty()) {
                    ret = i
                    break
                }
            }
            return ret
        }
    }
}