package com.jameshuang.logwindow.util

/**
 * @auther James Huang
 * @date 2018/12/14
 */
internal class LogMessage {
    companion object {
        val TYPE_VERBOSE = "V"
        val TYPE_DEBUG = "D"
        val TYPE_INFO = "I"
        val TYPE_WARN = "W"
        val TYPE_ERROR = "E"
    }

    var time = ""
    var pid = ""
    var tid = ""
    var type = ""
    var tag = ""
    var msg = ""
    var wholeMsg = ""
}