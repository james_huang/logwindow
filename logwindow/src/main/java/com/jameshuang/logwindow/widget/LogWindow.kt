package com.jameshuang.logwindow.widget

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.graphics.PixelFormat
import android.graphics.Point
import android.os.Build
import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.view.*
import android.widget.*
import com.jameshuang.logwindow.R
import com.jameshuang.logwindow.service.LoggerService
import com.jameshuang.logwindow.util.LogMessage
import java.util.*

/**
 * @auther James Huang
 * @date 2018/12/14
 */
internal class LogWindow {

    private val OPT_TIME = 0x1
    private val OPT_PID_TID = 0x2
    private val OPT_TYPE = 0x4
    private val OPT_TAG = 0x8
    private val OPT_ALL = 0xF
    private val FILTER_LEVEL_VERBOSE = 0
    private val FILTER_LEVEL_DEBUG = 1
    private val FILTER_LEVEL_INFO = 2
    private val FILTER_LEVEL_WARN = 3
    private val FILTER_LEVEL_ERROR = 4
    private val SYMBOL_SPACE = " "
    private val WINDOW_TYPE = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY else WindowManager.LayoutParams.TYPE_SYSTEM_ALERT

    private lateinit var mContext: Context
    private lateinit var mWindowMng: WindowManager
    private lateinit var mLogMutableLp: WindowManager.LayoutParams
    private lateinit var mBtnCloseMutableLp: WindowManager.LayoutParams
    private lateinit var mBtnDeleteMutableLp: WindowManager.LayoutParams
    private lateinit var mBtnBottomMutableLp: WindowManager.LayoutParams
    private lateinit var mBtnSettingsMutableLp: WindowManager.LayoutParams
    private lateinit var mBtnFilterMutableLp: WindowManager.LayoutParams
    private lateinit var mLogView: View
    private lateinit var mLayoutBtnClose: View
    private lateinit var mLayoutBtnDelete: View
    private lateinit var mLayoutBtnBottom: View
    private lateinit var mLayoutBtnSettings: View
    private lateinit var mLayoutBtnFilter: View
    private lateinit var mLvMsg: RecyclerView
    private lateinit var mAdapter: MsgAdapter
    private lateinit var mSpinnerAdapter: ArrayAdapter<CharSequence>
    private var mSettingsDlg: AlertDialog? = null

    private val mMsgList = Collections.synchronizedList<LogMessage>(ArrayList<LogMessage>())
    private val mFilterList = Collections.synchronizedList<LogMessage>(ArrayList<LogMessage>())
    private var mIsShowing = false
    private var mAutoScroll = true
    private var mTransMode = false
    private var mLimitY = 0
    private var mOptSettings = OPT_TIME or OPT_PID_TID or OPT_TYPE or OPT_TAG
    private var mFilterLevel = FILTER_LEVEL_VERBOSE
    private var mFilterString = ""

    constructor(context: Context) {
        mContext = context
        mWindowMng = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager

        initLp()
        setupView()
    }

    fun show() {
        if (mIsShowing) {
            return
        }

        mIsShowing = true
        mWindowMng.addView(mLogView, mLogMutableLp)
        mWindowMng.addView(mLayoutBtnClose, mBtnCloseMutableLp)
        mWindowMng.addView(mLayoutBtnDelete, mBtnDeleteMutableLp)
        mWindowMng.addView(mLayoutBtnSettings, mBtnSettingsMutableLp)
        mWindowMng.addView(mLayoutBtnFilter, mBtnFilterMutableLp)
    }

    fun dismiss() {
        if (!mIsShowing) {
            return
        }

        mIsShowing = false
        mWindowMng.removeView(mLogView)
        mWindowMng.removeView(mLayoutBtnClose)
        mWindowMng.removeView(mLayoutBtnDelete)
        mWindowMng.removeView(mLayoutBtnSettings)
        mWindowMng.removeView(mLayoutBtnFilter)
        if (!mAutoScroll) {
            mAutoScroll = true
            mWindowMng.removeView(mLayoutBtnBottom)
        }
    }

    fun relayout() {
        if (!mIsShowing) {
            return
        }

        dismiss()
        initLp()
        show()
        if (mAutoScroll) {
            if ((mFilterLevel != FILTER_LEVEL_VERBOSE) || !mFilterString.isEmpty()) {
                mLvMsg.scrollToPosition(mFilterList.size - 1)
            } else {
                mLvMsg.scrollToPosition(mMsgList.size - 1)
            }
        }
    }

    fun append(msg: LogMessage) {
        if (mMsgList.size == Int.MAX_VALUE - 1) {
            val view = mMsgList[0]
            mMsgList.remove(view)
        }

        mMsgList.add(msg)
        if ((mFilterLevel != FILTER_LEVEL_VERBOSE) || !mFilterString.isEmpty()) {
            if (isFilteredMessage(msg)) {
                mFilterList.add(msg)
            }
        }

        mAdapter.notifyDataSetChanged()
        if (mAutoScroll) {
            mLvMsg.post {
                if ((mFilterLevel != FILTER_LEVEL_VERBOSE) || !mFilterString.isEmpty()) {
                    mLvMsg.scrollToPosition(mFilterList.size - 1)
                } else {
                    mLvMsg.scrollToPosition(mMsgList.size - 1)
                }
            }
        }
    }

    private fun initLp() {
        val size = Point()
        mWindowMng.defaultDisplay.getSize(size)
        mLimitY = size.y / 3 * 2
        val flags = (WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                or View.SYSTEM_UI_FLAG_LAYOUT_STABLE)

        mLogMutableLp = WindowManager.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT,
                size.y / 3,
                WINDOW_TYPE,
                flags,
                PixelFormat.TRANSLUCENT
        )
        mLogMutableLp.gravity = Gravity.TOP or Gravity.LEFT
        mLogMutableLp.x = 0
        mLogMutableLp.y = mLimitY

        mBtnCloseMutableLp = WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WINDOW_TYPE,
                flags,
                PixelFormat.TRANSLUCENT
        )
        mBtnCloseMutableLp.gravity = Gravity.TOP or Gravity.LEFT
        mBtnCloseMutableLp.x = size.x - mContext.resources.getDimensionPixelSize(R.dimen.button_size)
        mBtnCloseMutableLp.y = mLogMutableLp.y - mContext.resources.getDimensionPixelSize(R.dimen.button_size)

        mBtnDeleteMutableLp = WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WINDOW_TYPE,
                flags,
                PixelFormat.TRANSLUCENT
        )
        mBtnDeleteMutableLp.gravity = Gravity.TOP or Gravity.LEFT
        mBtnDeleteMutableLp.x = mBtnCloseMutableLp.x - mContext.resources.getDimensionPixelSize(R.dimen.button_size)
        mBtnDeleteMutableLp.y = mBtnCloseMutableLp.y

        mBtnBottomMutableLp = WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WINDOW_TYPE,
                flags,
                PixelFormat.TRANSLUCENT
        )
        mBtnBottomMutableLp.gravity = Gravity.TOP or Gravity.LEFT
        mBtnBottomMutableLp.x = mBtnDeleteMutableLp.x - mContext.resources.getDimensionPixelSize(R.dimen.button_size)
        mBtnBottomMutableLp.y = mBtnCloseMutableLp.y

        mBtnSettingsMutableLp = WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WINDOW_TYPE,
                flags,
                PixelFormat.TRANSLUCENT
        )
        mBtnSettingsMutableLp.gravity = Gravity.TOP or Gravity.LEFT
        mBtnSettingsMutableLp.x = 0
        mBtnSettingsMutableLp.y = mBtnCloseMutableLp.y

        mBtnFilterMutableLp = WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WINDOW_TYPE,
                flags,
                PixelFormat.TRANSLUCENT
        )
        mBtnFilterMutableLp.gravity = Gravity.TOP or Gravity.LEFT
        mBtnFilterMutableLp.x = mBtnSettingsMutableLp.x + mContext.resources.getDimensionPixelSize(R.dimen.button_size)
        mBtnFilterMutableLp.y = mBtnCloseMutableLp.y
    }

    private fun setupView() {
        mLogView = LayoutInflater.from(mContext).inflate(R.layout.layout_log_window, null, false)
        mLogView.setOnTouchListener(mLogViewTouchListener)

        mLayoutBtnClose = LayoutInflater.from(mContext).inflate(R.layout.layout_btn_close, null, false)
        val btnClose = mLayoutBtnClose.findViewById<ImageButton>(R.id.btnClose)
        btnClose.setOnClickListener(mClickClose)

        mLayoutBtnDelete = LayoutInflater.from(mContext).inflate(R.layout.layout_btn_delete, null, false)
        val btnDelete = mLayoutBtnDelete.findViewById<ImageButton>(R.id.btnDelete)
        btnDelete.setOnClickListener(mClickDelete)

        mLayoutBtnBottom = LayoutInflater.from(mContext).inflate(R.layout.layout_btn_bottom, null, false)
        val btnBottom = mLayoutBtnBottom.findViewById<ImageButton>(R.id.btnBottom)
        btnBottom.setOnClickListener(mClickBottom)

        mLayoutBtnSettings = LayoutInflater.from(mContext).inflate(R.layout.layout_btn_settings, null, false)
        val btnSettings = mLayoutBtnSettings.findViewById<ImageButton>(R.id.btnSettings)
        btnSettings.setOnClickListener(mClickSettings)

        mLayoutBtnFilter = LayoutInflater.from(mContext).inflate(R.layout.layout_btn_filter, null, false)
        val btnFilter = mLayoutBtnFilter.findViewById<ImageButton>(R.id.btnFilter)
        btnFilter.setOnClickListener(mClickFilter)

        mSpinnerAdapter = ArrayAdapter.createFromResource(mContext, R.array.filter_level_list, R.layout.spinner_item)
        mSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        mLvMsg = mLogView.findViewById(R.id.lvMsg)
        mLvMsg.setOnTouchListener(mTouchLvMsg)
        mAdapter = MsgAdapter()
        mLvMsg.adapter = mAdapter
    }

    private fun setupTransMode() {
        if (mTransMode) {
            mLogView.background = mContext.getDrawable(R.drawable.bg_log_window_trans)
        } else {
            mLogView.background = mContext.getDrawable(R.drawable.bg_log_window)
        }
        mAdapter.notifyDataSetChanged()
    }

    private fun setupFilter() {
        val btnFilter = mLayoutBtnFilter.findViewById<ImageButton>(R.id.btnFilter)
        if ((mFilterLevel != FILTER_LEVEL_VERBOSE) || !mFilterString.isEmpty()) {
            btnFilter.setImageResource(R.drawable.btn_filter_hl)
            mFilterList.clear()
            for (msg in mMsgList) {
                if (isFilteredMessage(msg)) {
                    mFilterList.add(msg)
                }
            }
        } else {
            btnFilter.setImageResource(R.drawable.btn_filter_n)
        }
        mAdapter.notifyDataSetChanged()
    }

    private fun isFilteredMessage(msg: LogMessage): Boolean {
        val filterLevel: Int
        when (msg.type) {
            LogMessage.TYPE_DEBUG -> {
                filterLevel = FILTER_LEVEL_DEBUG
            }

            LogMessage.TYPE_INFO -> {
                filterLevel = FILTER_LEVEL_INFO
            }

            LogMessage.TYPE_WARN -> {
                filterLevel = FILTER_LEVEL_WARN
            }

            LogMessage.TYPE_ERROR -> {
                filterLevel = FILTER_LEVEL_ERROR
            }

            else -> {
                filterLevel = FILTER_LEVEL_VERBOSE
            }
        }

        if (filterLevel >= mFilterLevel) {
            if (!mFilterString.isEmpty()) {
                if (msg.tag.contains(mFilterString, true) || msg.msg.contains(mFilterString, true)) {
                    return true
                }
            } else {
                return true
            }
        }

        return false
    }

    private val mLogViewTouchListener = object : View.OnTouchListener {

        private val LONG_PRESS_TIME = 1000L
        private val LONG_PRESS_SCOPE = 20f

        private var origX = 0
        private var origY = 0
        private var origTouchX = 0f
        private var origTouchY = 0f
        private var click = false

        override fun onTouch(v: View?, event: MotionEvent?): Boolean {
            when (event?.action) {
                MotionEvent.ACTION_DOWN -> {
                    if (!click) {
                        click = true
                    }

                    mTransMode = true
                    setupTransMode()

                    origX = mLogMutableLp.x
                    origY = mLogMutableLp.y
                    origTouchX = event.rawX
                    origTouchY = event.rawY

                    return true
                }

                MotionEvent.ACTION_MOVE -> {
                    val distX = event.rawX.toInt() - origTouchX
                    val distY = event.rawY.toInt() - origTouchY
                    mLogMutableLp.y = origY + distY.toInt()
                    if (mLogMutableLp.y < 0) {
                        mLogMutableLp.y = 0
                    }
                    if (mLogMutableLp.y >= mLimitY) {
                        mLogMutableLp.y = mLimitY
                    }
                    mBtnCloseMutableLp.y = mLogMutableLp.y - mContext.resources.getDimensionPixelSize(R.dimen.button_size)
                    mBtnDeleteMutableLp.y = mBtnCloseMutableLp.y
                    mBtnBottomMutableLp.y = mBtnCloseMutableLp.y
                    mBtnSettingsMutableLp.y = mBtnCloseMutableLp.y
                    mBtnFilterMutableLp.y = mBtnCloseMutableLp.y

                    mWindowMng.updateViewLayout(mLogView, mLogMutableLp)
                    mWindowMng.updateViewLayout(mLayoutBtnClose, mBtnCloseMutableLp)
                    mWindowMng.updateViewLayout(mLayoutBtnDelete, mBtnDeleteMutableLp)
                    mWindowMng.updateViewLayout(mLayoutBtnSettings, mBtnSettingsMutableLp)
                    mWindowMng.updateViewLayout(mLayoutBtnFilter, mBtnFilterMutableLp)
                    if (!mAutoScroll) mWindowMng.updateViewLayout(mLayoutBtnBottom, mBtnBottomMutableLp)

                    if (distX >= LONG_PRESS_SCOPE || distY >= LONG_PRESS_SCOPE) {
                        click = false
                    }

                    return true
                }

                MotionEvent.ACTION_UP -> {
                    click = false
                    mTransMode = false
                    setupTransMode()
                    return true
                }
            }
            return false
        }
    }

    private val mTouchLvMsg = object : View.OnTouchListener {

        override fun onTouch(v: View?, event: MotionEvent?): Boolean {
            when (event?.action) {
                MotionEvent.ACTION_DOWN -> {
                    if (mAutoScroll) {
                        mAutoScroll = false
                        mWindowMng.addView(mLayoutBtnBottom, mBtnBottomMutableLp)
                    }
                }
            }
            return false
        }
    }

    private val mClickClose = View.OnClickListener {
        val intent = Intent(mContext, LoggerService::class.java)
        val bundle = Bundle()
        bundle.putInt(LoggerService.KEY_CMD, LoggerService.CMD_STOP)
        intent.putExtras(bundle)

        mContext.startService(intent)
    }

    private val mClickDelete = View.OnClickListener {
        mMsgList.clear()
        mFilterList.clear()
        mAdapter.notifyDataSetChanged()
    }

    private val mClickBottom = View.OnClickListener {
        if (!mAutoScroll) {
            mAutoScroll = true
            mWindowMng.removeView(mLayoutBtnBottom)
            if ((mFilterLevel != FILTER_LEVEL_VERBOSE) || !mFilterString.isEmpty()) {
                mLvMsg.scrollToPosition(mFilterList.size - 1)
            } else {
                mLvMsg.scrollToPosition(mMsgList.size - 1)
            }
        }
    }

    private val mClickSettings = View.OnClickListener {
        if (mSettingsDlg != null && mSettingsDlg!!.isShowing) {
            return@OnClickListener
        }

        val view = LayoutInflater.from(mContext).inflate(R.layout.layout_option_settings, null, false)
        val boxTime = view.findViewById<CheckBox>(R.id.boxTime)
        val boxPidTid = view.findViewById<CheckBox>(R.id.boxPidTid)
        val boxType = view.findViewById<CheckBox>(R.id.boxType)
        val boxTag = view.findViewById<CheckBox>(R.id.boxTag)
        boxTime.isChecked = (mOptSettings and OPT_TIME == OPT_TIME)
        boxPidTid.isChecked = (mOptSettings and OPT_PID_TID == OPT_PID_TID)
        boxType.isChecked = (mOptSettings and OPT_TYPE == OPT_TYPE)
        boxTag.isChecked = (mOptSettings and OPT_TAG == OPT_TAG)

        mSettingsDlg = AlertDialog.Builder(mContext)
                .setTitle(R.string.tips_select_showing)
                .setView(view)
                .setPositiveButton(mContext.getString(android.R.string.ok)) { dialog, which ->
                    mOptSettings = 0
                    if (boxTime.isChecked) mOptSettings = mOptSettings or OPT_TIME
                    if (boxPidTid.isChecked) mOptSettings = mOptSettings or OPT_PID_TID
                    if (boxType.isChecked) mOptSettings = mOptSettings or OPT_TYPE
                    if (boxTag.isChecked) mOptSettings = mOptSettings or OPT_TAG

                    mAdapter.notifyDataSetChanged()
                }
                .setNegativeButton(mContext.getString(android.R.string.cancel), null)
                .create()
        mSettingsDlg!!.window.setType(WINDOW_TYPE)
        mSettingsDlg!!.show()
    }

    private val mClickFilter = View.OnClickListener {
        if (mSettingsDlg != null && mSettingsDlg!!.isShowing) {
            return@OnClickListener
        }

        val view = LayoutInflater.from(mContext).inflate(R.layout.layout_filter_settings, null, false)
        val spLevel = view.findViewById<Spinner>(R.id.spLevel)
        val edtFilter = view.findViewById<EditText>(R.id.edtFilter)
        spLevel.adapter = mSpinnerAdapter
        spLevel.setSelection(mFilterLevel)
        edtFilter.setText(mFilterString)

        mSettingsDlg = AlertDialog.Builder(mContext)
                .setTitle(R.string.filter)
                .setView(view)
                .setPositiveButton(mContext.getString(android.R.string.ok)) { dialog, which ->
                    mFilterLevel = spLevel.selectedItemPosition
                    mFilterString = edtFilter.text.toString()
                    setupFilter()
                }
                .setNegativeButton(mContext.getString(R.string.reset)) { dialog, which ->
                    mFilterLevel = FILTER_LEVEL_VERBOSE
                    mFilterString = ""
                    setupFilter()
                }
                .create()
        mSettingsDlg!!.window.setType(WINDOW_TYPE)
        mSettingsDlg!!.show()
    }

    inner class MsgAdapter : RecyclerView.Adapter<MsgAdapter.ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.layout_msg, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            if ((mFilterLevel != FILTER_LEVEL_VERBOSE) || !mFilterString.isEmpty()) {
                return mFilterList.size
            } else {
                return mMsgList.size
            }
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val msg = if ((mFilterLevel != FILTER_LEVEL_VERBOSE) || !mFilterString.isEmpty()) {
                mFilterList[position]
            } else {
                mMsgList[position]
            }

            when (msg.type) {
                LogMessage.TYPE_ERROR -> {
                    if (mTransMode) {
                        holder.tvMsg.setTextColor(mContext.getColor(R.color.colorMsgErrTrans))
                    } else {
                        holder.tvMsg.setTextColor(mContext.getColor(R.color.colorMsgErr))
                    }
                }

                else -> {
                    if (mTransMode) {
                        holder.tvMsg.setTextColor(mContext.getColor(R.color.colorMsgNormalTrans))
                    } else {
                        holder.tvMsg.setTextColor(mContext.getColor(R.color.colorMsgNormal))
                    }
                }
            }

            var text = ""
            if ((mOptSettings and OPT_TIME == OPT_TIME)) {
                text += msg.time + SYMBOL_SPACE
            }
            if ((mOptSettings and OPT_PID_TID == OPT_PID_TID)) {
                text += msg.pid + SYMBOL_SPACE + msg.tid + SYMBOL_SPACE
            }
            if ((mOptSettings and OPT_TYPE == OPT_TYPE)) {
                text += msg.type + SYMBOL_SPACE
            }
            if ((mOptSettings and OPT_TAG == OPT_TAG)) {
                text += msg.tag + SYMBOL_SPACE
            }
            text += msg.msg
            holder.tvMsg.text = text
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var tvMsg: TextView = view.findViewById(R.id.tvMsg)
        }
    }
}