package com.jameshuang.logwindow.service

import android.app.Service
import android.content.Intent
import android.content.res.Configuration
import android.os.Handler
import android.os.IBinder
import com.jameshuang.logwindow.util.LogParserUtil
import com.jameshuang.logwindow.util.RuntimeLogUtil
import com.jameshuang.logwindow.widget.LogWindow

/**
 * @auther James Huang
 * @date 2018/12/14
 */
internal class LoggerService : Service() {

    private val mHandler = Handler()
    private lateinit var mLogWindow: LogWindow
    private lateinit var mRuntimeLogUtil: RuntimeLogUtil

    companion object {
        val KEY_CMD = "command"
        val CMD_START = 0
        val CMD_STOP = -1
    }

    override fun onBind(p0: Intent?): IBinder? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onCreate() {
        super.onCreate()

        mLogWindow = LogWindow(this)
        mRuntimeLogUtil = RuntimeLogUtil.Builder().build(mRuntimeLogListener)
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        val bundle = intent?.extras
        val cmd = bundle?.getInt(KEY_CMD, CMD_START)

        when (cmd) {
            CMD_START -> {
                mLogWindow.show()
                mRuntimeLogUtil.start()
            }

            CMD_STOP -> {
                mRuntimeLogUtil.stop()
                mLogWindow.dismiss()
                stopSelf()
                return Service.START_NOT_STICKY
            }
        }

        return Service.START_STICKY
    }

    override fun onConfigurationChanged(newConfig: Configuration?) {
        super.onConfigurationChanged(newConfig)
        mLogWindow.relayout()
    }

    private val mRuntimeLogListener = object : RuntimeLogUtil.RuntimeLogListener {
        override fun onReadLine(msg: String) {
            val logMsg = LogParserUtil.parse(msg)
            if (logMsg != null) {
                mHandler.post {
                    mLogWindow.append(logMsg)
                }
            }
        }
    }
}